# Papelaria UnB - vitória

Luis Henrique Luz Costa       18/0066161

## Descrição

Em um bairro singelo Vitória começa seus primeiros passos empreendendo na comunidade local. De inicio estava fluindo bem. Porém após algum tempo a demanda foi aumentando e sua forma de gerênciar a padaria se tornara improdutiva.
Fui indicado para ajuda-lá a programar o sistema e **todas as vendas serão realizadas pelo computador operado por uma funcionária**.

### Modo venda
Há duas formas de acessar o modo venda. A primeira é como **visitante**, que permite que você pesquise um produto em estoque e adicione ao carrinho porém para efetuar a compra deve realizar um login. A segunda forma é pelo **cadastro** login e senha. Dessa forma você possui acesso total ao sistema, como cliente, e pode fazer pedidos dos produtos em estoque.

### Modo recomendação
É definido com a interação do usuário ao realizar compras. No modo recomendação ele ordena produtos e mostra-os de maior interesse pelo usuário.

### Modo estoque
O modo estoque permite cadastrar produtos por categorias.