#ifndef MENU_HPP
#define MENU_HPP

using namespace std;

class Menu {
    private:
        //Atributos

    public:
        //Métodos
        Menu();

        //Outros métodos
        void menu_principal();
        void menu_venda();
        void menu_recomendacao();
        void menu_estoque();
        

};

#endif