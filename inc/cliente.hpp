#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Cliente {

    private:
        //Atributos
        string nome;
        string cpf;
        string telefone;
        string email;
        string senha;
        string socio;

        vector <Cliente *> clientes;

    public:
        //Métodos
        Cliente();
        Cliente(string nome, string cpf, string telefone, string email, string socio);
        ~Cliente();

        void set_nome(string nome);
        string get_nome();
        void set_cpf(string cpf);
        string get_cpf();
        void set_telefone(string telefone);
        string get_telefone();
        void set_email(string email);
        string get_email();
        void set_senha(string senha);
        string get_senha();
        void set_socio(string socio);
        string get_socio();

        //Outros métodos
        void cadastra_dados_cliente();
        //void imprime_lista_clientes();
        



};

#endif