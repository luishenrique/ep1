#include <iostream>
#include "cliente.hpp"

Cliente::Cliente() {}

Cliente::Cliente(string nome, string cpf, string telefone, string email, string socio) {
    set_nome(nome);
    set_cpf(cpf);
    set_telefone(telefone);
    set_email(email);
    set_socio(socio);
}

Cliente::~Cliente() {}

void Cliente::set_nome(string nome) {
    this->nome = nome;
}
string Cliente::get_nome() {
    return nome;
}
void Cliente::set_cpf(string cpf) {
    this->cpf = cpf;
}
string Cliente::get_cpf() {
    return cpf;
}
void Cliente::set_telefone(string telefone){
    this->telefone = telefone;
}
string Cliente::get_telefone() {
    return telefone;
}
void Cliente::set_email(string email) {
    this->email = email;
}
string Cliente::get_email() {
    return email;
}
void Cliente::set_senha(string senha) {
    this->senha = senha;
}
string Cliente::get_senha() {
    return senha;
}
void Cliente::set_socio (string socio) {
    this->socio = socio;
}
string Cliente::get_socio() {
    return socio;
}
