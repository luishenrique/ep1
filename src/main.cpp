#include <iostream>
#include <unistd.h>
#include <vector>
#include <fstream>
#include <stdio.h>
#include "menu.hpp"
#include "cliente.hpp"

string getString(){
    string valor;
    getline(cin, valor);
    return valor;
}

template <typename T1>

T1 getInput(){
    while(true){
        T1 valor;
        cin >> valor;
        if(cin.fail()){
            cin.clear();
            cin.ignore(32767,'\n');
            cout << "Entrada inválida! Insira novamente: " << endl;
        }
        else{
            cin.ignore(32767,'\n');
            return valor;
        }
    }
}

int main() {

    Menu menu;
    //Cliente cliente;

    int comando = -1;
    int op = -1;

    string linha;
    int count = 0;

    string nome;
    string cpf;
    string telefone;
    string email;
    string senha;
    string socio;
    string pesquisar;

    //Arquivo que armazena dados do cliente
    ofstream dados_cadastro;
    ifstream exibir_dados;

    //Arquivo que armazena dados do produto
    ofstream dados_produto;
    ifstream exibir_dados_produto;


    while(comando != 0) {
        menu.menu_principal();
        comando = getInput<int>();

        switch (comando)
        {
        case 1:
            menu.menu_venda();
            op = getInput<int>();

            switch (op) {
            case 1:
                system("clear");
                //Efetuar login
                cout << "=========================" << endl;
                cout << "/         Login         /" << endl;
                cout << "=========================" << endl;
                cout << "E-mail: ";
                email = getString();
                cout << "Senha: "; 
                senha = getString();

                //Validação do usuario
                exibir_dados.open("./doc/dados_cliente.txt");

                while(!exibir_dados.eof()) {
                    getline(exibir_dados, linha);

                    if(email == linha) {
                        count++;
                    }
                    if(senha == linha) {
                        count++;
                    }
                }

                cout << count << endl;
                 
                system("clear");

                if(count == 2) {
                    cout << "===========================" << endl;
                    cout << "/        Bem-vindo        /" << endl;
                    cout << "===========================" << endl;
                    cout << "Pesquisar: ";
                    pesquisar = getString();
                    
                }

                if(count != 2) {
                    cout << "Usuário não cadastrado!" << endl;
                    count = 0;

                    sleep(2);                    
                }

                break;

            case 3:
                system("clear");
                cout << "CADASTRO" << endl;
                cout << "-------------" << endl;
                cout << "Nome: ";
                nome = getString();
                cout << "CPF: ";
                cpf = getString();
                cout << "Telefone: ";
                telefone = getString();
                cout << "E-mail: ";
                email = getString();
                cout << "Senha para login: ";
                senha = getString();
                cout << "\n-----------------------------------" << endl;
                cout << "Para obter 15% de desconto em todos \nos produtos torne-se sócio :D\n" << endl;
                cout << "Deseja se tornar sócio[S/N]?: ";
                socio = getString();

                //ofstream dados_cadastro;
                dados_cadastro.open("./doc/dados_cliente.txt", ios::app);

                if(dados_cadastro.is_open()) {
                    
                    dados_cadastro << nome << endl; 
                    dados_cadastro << cpf << endl;
                    dados_cadastro << telefone << endl;
                    dados_cadastro << email << endl;
                    dados_cadastro << senha << endl;
                    dados_cadastro << socio << endl;
                    dados_cadastro << "----------------------" << endl;

                    dados_cadastro.close();
                }

                system("clear");
                    cout << "Cadastro realizado com sucesso!" << endl;
                sleep(2);

                break;
            
            case 0:
                break;

            default:
                system("clear");
                cout << "Comando inválido, insira novamente!\n" << endl;
                sleep(2);
                break;
            }
            

            break;

        case 0:
            break;
        
        default:
            system("clear");
            cout << "Comando inválido, insira novamente!\n" << endl;
            sleep(2);
            break;
        }
    }

    return 0;
}